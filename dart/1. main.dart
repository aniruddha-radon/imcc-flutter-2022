// dart starts from a function called as main

// void -> means nothing
// in dart function names are prefixed with the datatype of
// the value they return

// main is the name of the function
// List -> is a datatype that represents a list
// <> generic datatype
// String -> represents text
// List<String> -> List of String elements
// args -> variable name
void main(List<String> args) {
  // print is a built in function that prints the passed string to the console
  print("our first dart program runs");
}

// to run the dart program run -> dart <filename>.dart