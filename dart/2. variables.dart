void main(List<String> args) {
  // numbers
  int number; // creates a non-nullable integer variable called number
  int ?primeNumber; // creates a nullable integer variable called primeNumber
  print(primeNumber); // prints null.

  // number = null; // not allowed as number is not nullable
  number = 10; // assigning a value to the variable number
  primeNumber = 7; // re-assigning (changing) value of variable
  print(number); // 10

  int twenty = 20; // assignment + declaration.
  print(twenty); // 20
  print(twenty.runtimeType);

  double pi = 3.14;
  // num can be assigned both double and int
  num a = 5;
  num b = 6.14;
  b = 6;
  a = 7.8;
  print(a.runtimeType);
  print(pi.runtimeType);
  // more reading: https://dart.dev/guides/language/numbers

  // strings
  String singleQuoted = 'this is a string';
  String doubleQuoted = "this is also a string";
  String multiLine = """I have  
                        more than 
                        one
                        line
                     """;
  print(multiLine.runtimeType);

  // booleans
  bool isRaining = false;
  bool hadLunch = true;
  print(hadLunch.runtimeType);

  Null nullVal = null; // is the default value of a nullable variable.
  print(nullVal.runtimeType);

  // dynamic variables can be assigned ANY value
  dynamic anyValue = "I am string";
  anyValue = 5;
  anyValue = false;
  anyValue = null;
  print(anyValue.runtimeType);

  // type inference using var
  var numb = 5; // type of num will be int
  print(numb.runtimeType);
  // num = "abcd"; // not allowed as dart has INFERED that typeof num is int
  // var is the preferred way to store values from functions

  // Maps
  // Map<DataType of keys, DataType of values>
  Map<String, dynamic> person =  {
    "name": "Aniruddha",
    "age": 30,
    "brain": false,
    "girlfriend": null,
    "address": {
      "city": "Pune",
      "country": "IN"
    }
  };
  print(person.runtimeType);

  // List
  // List<Datatype of elements>
  List<dynamic> anything = [1, "abcd", null, true, person, [1, 2, 3]];
  List<int> primes = [2, 3, 5, 7, 11];
  List<Map<String, dynamic>> people = [
    { "name": "Aniruddha" },
    { "name": "Ameya" },
  ];
  print(anything.runtimeType);

  // functions
  void add(int a, int b) {
    print(a + b);
  }

  print(add.runtimeType);
  // add.call(1, 2);

  const PI = 3.14;
  final abs;
  abs = 5;
  // const needs to be assigned a value while declaration
  // final and const, once assigned a value cannot be re-assigned a value

  // self study on enum, runes
}