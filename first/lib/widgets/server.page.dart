import 'package:flutter/material.dart';

class ServerPage extends StatelessWidget {
  final List<String> servers = ['Development', 'UAT', 'Testing', 'Production'];

  ServerPage({ super.key });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Server'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
        // ignore: prefer_const_literals_to_create_immutables
        children: servers.map((l) => Server(label: l)).toList()
      ),
      )
    );
  }
}

class Server extends StatelessWidget {
  final String label;

  const Server({super.key, required this.label});

  @override
  Widget build(BuildContext context) {
    return Text(label);
  }
}