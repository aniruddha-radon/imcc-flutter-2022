import 'package:api_handling/pages/user-details.page.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class User {
  final String name;
  final int id;

  User({required this.name, required this.id});

  // this method returns an object of type User created from a JSON
  factory User.fromJson(Map<String, dynamic> userJson) {
    return User(name: userJson['name'], id: userJson['id']);
  }
}

class UserPage extends StatefulWidget {
  const UserPage({super.key});

  @override
  State<UserPage> createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  late Future<List<User>> userFuture;

  void showUserData(int id) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => UserDetails(id: id),
      ),
    );
  }

  Future<List<User>> getUsersData() async {
    try {
      var response = await http.get(
        Uri.parse("https://jsonplaceholder.typicode.com/users"),
      );

      var userArray = jsonDecode(response.body);

      List<User> users = [];

      for (var i = 0; i < userArray.length; i++) {
        users.add(User.fromJson(userArray[i]));
      }

      return users;
    } catch (e) {
      return [
        User(name: "Aniruddha", id: 1),
        User(name: "Aniruddha 2", id: 2),
        User(name: "Aniruddha 3", id: 3)
      ];
    }
  }

  @override
  void initState() {
    super.initState();
    userFuture = getUsersData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("User List")),
      body: FutureBuilder(
          future: userFuture,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data!.isEmpty) {
                return const Center(child: Text("No rows to display."));
              }

              return Column(
                children: snapshot.data!
                    .map((user) => UserRow(
                          user: user,
                          showUserData: showUserData,
                        ))
                    .toList(),
              );
            }

            return const Center(child: Text("Loading..."));
          }),
    );
  }
}

class UserRow extends StatelessWidget {
  final User user;
  final Function? showUserData;
  const UserRow({super.key, required this.user, this.showUserData});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (showUserData != null) {
          showUserData!(user.id);
        }
      },
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
        ),
        margin: const EdgeInsets.all(8.0),
        child: Text(user.name),
      ),
    );
  }
}
