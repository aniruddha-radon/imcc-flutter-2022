import 'package:api_handling/pages/user.page.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class UserDetails extends StatefulWidget {
  final int id;
  const UserDetails({super.key, required this.id});

  @override
  State<UserDetails> createState() => _UserDetailsState();
}

class _UserDetailsState extends State<UserDetails> {
  late Future<User> userFuture;

  Future<User> getUserDetails() async {
    try {
      var response = await http.get(
        Uri.parse("https://jsonplaceholder.typicode.com/users/${widget.id}"),
      );

      var userJson = jsonDecode(response.body);

      User user = User.fromJson(userJson);

      return user;
    } catch (e) {
      return User(name: 'name', id: 112);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userFuture = getUserDetails();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("User Details"),
      ),
      body: FutureBuilder(
          future: userFuture,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Center(
                child: Text(
                  widget.id.toString(),
                ),
              );
            }

            return const Center(
              child: Text("Loading..."),
            );
          }),
    );
  }
}
