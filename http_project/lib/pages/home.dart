import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http_project/API/users.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late Future<List<User>> userFuture;

  Future<List<User>> fetchAlbum() async {
    final response =
        await http.get(Uri.parse('https://jsonplaceholder.typicode.com/users'));
    
    var arr = jsonDecode(response.body);
    List<User> users = [];

    for(var i = 0; i < arr.length; i++){
      User user = User.fromJson(arr[i]);
      users.add(user);
    }

    
    return users;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userFuture = fetchAlbum();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Users")),
      body: FutureBuilder<List<User>>(
          future: userFuture,
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data != null) {
              return Column(
                children: [
                  Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(8.0),
                    margin: const EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black38)),
                    child: Text(snapshot.data!.first.name),
                  ),
                  Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black38)),
                    margin: const EdgeInsets.all(8.0),
                    child: Text("User Name"),
                  )
                ],
              );
            }
            return Text("waiting");
          }),
    );
  }
}
