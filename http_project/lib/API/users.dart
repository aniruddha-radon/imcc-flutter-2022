class User {
  final String name;

  User({ required this.name });

  factory User.fromJson(Map<String, dynamic> userJson) {
    return User(name: userJson['name']);
  }

  static getUsers(List<Map<String, dynamic>> userArr) {
    print(userArr);
    List<User> users = [];
    for(var i = 0; i < userArr.length; i++){
      User user = User.fromJson(userArr[i]);
      users.add(user);
    }

    // print(users);
    return users;
  }

}

